
/* 
Ever wondered how much a "lifetime supply of Tea you will need?"
Let's find out!!!

You have a function called howManyTeas. It takes 3 arguments -- current age, expected end of life age, number of teas per day.
Calculate how many teas you will need until the end of life.
Return the number of teas as a result.
*/




var howManyTeas = function(age, end_age, teas_day){
    var years= end_age - age
    var yearTea= teas_day*365
    
    return years * yearTea
}

module.exports = {
    howManyTeas
}