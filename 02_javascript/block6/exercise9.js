
// Write a function called sumAll that takes one argument, an object, and sums all it's values. 
// If no object is provided or if the object is empty sumAll should return 0.

//======================  EXAMPLE  ========================
sumAll({a: 20, b: 26, c: 0})
46 // <======  EXPECTED OUTPUT
sumAll({a: 20, b: 26, c: -56})
-10 // <======  EXPECTED OUTPUT
sumAll({}) 
0 // <======  EXPECTED OUTPUT
sumAll() 
0 // <======  EXPECTED OUTPUT
//=========================================================  


const sumAll = (obj)=>{

}

module.exports = {
    sumAll
}