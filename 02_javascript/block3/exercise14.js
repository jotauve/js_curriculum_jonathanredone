// write a function called budgetTracker which helps you keep track of your expenses while on vacation...
// you went on vacation to Japan and now you came back and you need to check your finance a bit and in order 
// to do that you need to find out:
// the average expense per day in dollars, 
// each element of the array will be the daily expense in Japanese yens
// the conversion rate for yen to dollars is 0.0089
// return the daily average

const budgetTracker = (array)=>{
  var totalYen = 0
  let dollarValue=0.0089
  var days=0
  array.forEach(ele  => {
    totalYen += Number(ele)
    days++
  })
  var totalSspent = totalYen * dollarValue
 var average = (totalSspent/days)
 

 return Math.round(average)
}
module.exports = {
  budgetTracker
}