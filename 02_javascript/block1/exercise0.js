/*
Declaring variables:
Please declare a variable 'apple' and assign to it a value of 5
Declare a variable 'apple2' and assign to it a value of 10
Declare a variable 'basket' and assign to it sum of apple and apple2
Declare a variable 'myName' and assign your name as a value
*/
// write your code after this line
var apple = 5
var apple2 = 10
basket = apple + apple2
var myName = "Jonathan"


// please never modify module.exports for the solutions since it is used for the testing
module.exports = {
    apple, apple2, basket, myName
}