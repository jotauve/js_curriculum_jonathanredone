// Write a function called swap that takes one argument, an object, and  
// returns another object where the key/value pairs have been swapped. 
// The original object should not be modified.


const swap=(obj)=>{
var changed = {}
for (ele in obj){
    changed[obj[ele]] = ele
}
return changed

}


module.exports= {
    swap
}