// there are no test for this exercise
// syntax with await 
async function getCurrentWeather (){

    let lat,long
  
    navigator.geolocation.getCurrentPosition( async (position) => {
      lat = position.coords.latitude
      long = position.coords.longitude
  
      let response  = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=16909a97489bed275d13dbdea4e01f59`)
      let info = await response.json() 
      console.log(info )
   });
  }
  
  // syntax with .then
  async function getCurrentWeather (){
  
    let lat,long
  
    navigator.geolocation.getCurrentPosition( async (position) => {
      lat = position.coords.latitude
      long = position.coords.longitude
  
      fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=16909a97489bed275d13dbdea4e01f59`).then((r)=> {
         return r.json().then((info)=> {
          console.log(info)
       })
      }).catch((err)=> {
         console.log(err);
      })
   })
  }
module.exports = {
    getCurrentWeather
}