
/* 
Create a function called isEven which takes 1 argument and returns true if the argument passed is even and false if is odd.
*/



var isEven = function(arg) {
    var even = (arg%2 ==0)
	return even
}

module.exports = {
    isEven
}