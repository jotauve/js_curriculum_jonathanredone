// create a function called take_and_push which takes 3 arguments, an array and two numbers.
// the second and third arguments are representing indexes
// you need to take the elements in the array that are at the given indexes and push them to a new array.
// then return this new array.


const take_and_push=(array, i,j)=>{
var newArr=[]
newArr.push(array[i])
newArr.push(array[j])
return newArr
}


module.exports = {
    take_and_push,
}
