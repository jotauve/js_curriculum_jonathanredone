// Write a function called stringChop that chops a string into chunks of a 
//given length and returns an array with these chunks.

// The function takes 2 arguments -- the first one being the string to chop, and the second one a number that will 
//be the size of you chunks. If the second argument is not provided or it is 0 then your
//function should return the full original string inside an array.


function stringChop (str, len) {
    const size = Math.ceil(str.length/len)
    const r = Array(size)
    let offset = 0
    
    for (let i = 0; i < size; i++) {
      r[i] = str.substr(offset, len)
      offset += len
    }
    
    return r
  }





module.exports = {
    stringChop
}