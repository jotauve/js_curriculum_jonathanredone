// write a function called isEven which takes an array as argument and checks 
// if each element is even or odd.
// inside the function declare a variable called count which keeps track of 
// how many even numbers there are.
// once done return count
// see example below for how to use a basic if statement here.




const isEven =(array)=>{
  var count = 0
array.forEach(ele=>{
  if (ele%2==0)
  count++
}) 
 return count
}

module.exports = {
  isEven
}