
/*
Make a function which takes 2 arguments, year of birth and current year and returns the following message, replacing the word *'DAYS'* with the actual value
*/



var howManyDays = function (dob, now){
   var years = now-dob
   var days = years * 365
    return `you have lived for ${days} days already!`
}
module.exports = {
    howManyDays
}