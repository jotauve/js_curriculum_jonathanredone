
// write a function called filter that takes 3 arguments, an array, a data
// type and a minLength.
// it will loop through the array and return a new array where the elements
// are NOT of the type of passed data type
// and they have a length equal to or bigger than minLength

const filter=(array, data, minLength)=>{
var newArr=[]
array.forEach(ele =>{
    if (ele.length >= minLength){
        if (typeof ele != data){
        newArr.push(ele)}

    }
})
return newArr
}
module.exports = {
    filter
}