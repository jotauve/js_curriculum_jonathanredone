
// create a function called findPosition which takes two arguments, an array and an element and it returns
// the index of the given element.


const findPosition = (arr, ele)=>{
  var i = arr.indexOf(ele)
 return i
}


module.exports = {
    findPosition
}