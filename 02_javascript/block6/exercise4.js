// Write a function called addToList that takes one argument, an array of movie titles
// then create a new empty array called movieList.
// Loop through the array of movie titles and push them inside the movieList array as objects.
// each object will have two key/value pairs with titles and ids: 
// movie title from the array becomes the value of 'title' key in the object
// index of the element in the array should be the value for the id key in the object
// Then return movieList.

const addToList=(array)=>{
    var movieList= []
    array.forEach((ele, idx) => {
        movieList.push({title: ele, id: idx})
    })
     
return movieList
}

/*arg is an array of movie titles
and an empty array have been created
we have to loop through the first Movie titles array
and push the titles inside of the empty created array
the objects will have two values:key=title / value=id 
movie title in the first will become the key name in object index
the valeu of the movie title, is the index they occupy
then, return movielist*/










module.exports = {
    addToList
}