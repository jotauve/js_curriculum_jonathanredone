// create a function called isThere which takes an array as first argument and a string as second,
// it needs to check if the string is inside the array and returns true if it is and false if it's not.


const isThere =(arr,str)=>{
var check = arr.includes(str)
return check
}


module.exports ={
    isThere
}


