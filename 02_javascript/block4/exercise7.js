// write a function called booleanChecker, it takes two arguments, one array 
// and one maxCapacity which is a number
// create an empty array called bool inside your function
// loop through the provided array and every time you find a boolean push it 
// to our bool array until you reach the maxCapacity:
// if the maxCapacity argument is missing it should default to unlimited;



const booleanChecker=(array, maxCapacity)=>{
var bool=[]
array.forEach(ele=>{
  if (typeof ele == typeof(true)){
   if (bool.length!=maxCapacity){
    bool.push(ele)
}
}
})
return `${bool.length} booleans were found ${bool.toString()}`
}

module.exports = {
  booleanChecker
}
