// write a function called getIndex which finds the index of an element in an array of objects, 
// the objects have multiple key/value pairs so your function need to be flexible enough to find by any of them.
// it should return the index of the found element or -1 if is not there.

// to pass the test your function should take 3 arguments:
// array of objects, 
//name of the key 
//content of the value to search for, 
// so if they match it returns the index of the object containing this matching key:value pair


const getIndex=(array,key,value)=>{
  var idx = -1
  for(var i=0;i<array.length; i++){
      if (array[i][key] == value){
        idx = i
    } 

    }
    return idx 
}


module.exports ={
    getIndex
}


