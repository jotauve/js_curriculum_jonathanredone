// write function called twoArrays which takes two arrays as arguments, 
// these arrays are of the same length
// we need to count matches (same elements) at the same positions across
// these two arrays
// inside function declare a variable called count
// it then loops and compare each element of one array with that of the other, 
// in pairs with same indexes and every time it finds a match it increases the 
// number of count by one then return count


const twoArrays=(arr, arr2)=>{
count = 0
arr.forEach((elem,ind)=>{
if (elem===arr2[ind])
count ++
})
return count
}

module.exports ={
    twoArrays
}