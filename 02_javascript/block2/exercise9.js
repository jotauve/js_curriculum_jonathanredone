// create a function called concatenator which takes two arrays as the arguments
// and then using Array.concat combine them into a single array and return.


const concatenator = (arr, arr2) => {

 return arr.concat(arr2)
}

module.exports = {
    concatenator,
}
