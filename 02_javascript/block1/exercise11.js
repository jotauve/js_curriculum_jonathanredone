
/*
It's hot out! Let's make a converter based on the steps here:
http://www.mathsisfun.com/temperature-conversion.html

Make a function toFahr which takes one argument -- a temperature in Celsius (number) -- to convert it to
 fahrenheit and output the result.

Make a function toCelsius which takes one argument -- a temperature in Fahrenheit (number) --  to convert it to
 celsius and output the result.

Use Math.round() to round the results before returning them. Example of Math.round: Math.round(33.3) will give back 33

*/




var toCelsius = function (fahrenheit){
    var temp= ((fahrenheit - 32) * 5)/ 9
    var finalTemp = Math.round(temp)

    return finalTemp
}
var toFahr = function (celsius){
    var temp = (((celsius/ 5)*9)+32)
    var finalTemp= Math.round(temp)
    
    return finalTemp
}

module.exports = {
    toCelsius, 
    toFahr,
}