// Create a function called checker that loops through a string and checks how 
// many occurrences of commas and question marks there are in it.
// it then returns a string with the numbers of commas and question marks
// please make sure to use the appropriate form, so if for instance there are 
// more than one commas / question marks you should use plural 
// like commas / question marks, otherwise singular: comma / question mark.


const checker=(string)=>{
  var commaCount= string.split(",").length - 1;
  var questionCount=string.split("?").length - 1;
  var comma =''
  var question =''
if(commaCount<1){
  comma = '0 comma'}
if (commaCount==1){
  comma = '1 comma'}
if (commaCount>1){
  comma = `${commaCount} commas`}

if(questionCount<1){
  question = '0 mark'}
if (questionCount==1){
  question = '1 question mark'}
if (questionCount>1){
  question = `${questionCount} question marks`}

return `${comma}, ${question}`
}





module.exports = {
  checker
}



