// write a function called lowerCaseLetters which takes a string as argument.
// the string will contain some upper case letters and some numbers, 
// then create a new variable which needs to contain this string without numbers all in lowercase and 
// with spaces between words
// once done return the resulted string 

// original sentence = An2323t2323one32llo123455Likes567323Play323ing567243G2323a3mes345

const lowerCaseLetters=(string)=>{
 var string2=''
 for (character of string){
   if(isNaN(character)){
     if (character == character.toUpperCase()){
       string2 += ' '+character
     }else{
       string2+=character
     }
    }
  }
  return string2.trim().toLowerCase()
   
}

module.exports = {
  lowerCaseLetters
}