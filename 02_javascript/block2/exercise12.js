// create a function called isThere which takes an array as first argument and a string as second,
// it needs to check if the string is inside the array and returns true if it is and false if it's not.
// now do the same exercise using the method includes only this time you should return false if the
// color is there and true if it's not!

const isThere=(array, string)=>{
var test = !array.includes(string)
return test
}



module.exports ={
    isThere
}