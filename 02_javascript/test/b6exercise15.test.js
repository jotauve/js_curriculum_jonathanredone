var assert = require("chai").assert;
var sort = require("../block6/exercise15.js").sort;

describe("#test1", function () {
  it("sort should be a function", function () {
    assert.typeOf(sort, "function");
  });
});

describe("#test2", function () {
  it("sort should be { a: 1, e: 1, c: 3, d: 4, f: 4,b: 20 }", function () {
    var obj = sort({ a: 1, b: 20, c: 3, d: 4, e: 1, f: 4 });
    assert.deepEqual(Object.entries(sort({ a: 1, b: 20, c: 3, d: 4, e: 1, f: 4 })), Object.entries({
      a: 1,
      e: 1,
      c: 3,
      d: 4,
      f: 4,
      b: 20,
    })
    );
  });
});

describe("#test3", function () {
  it("sort should be { b: 3, age: 22, a: 99, year: 1999 ", function () {
    var obj = sort({ age: 22, year: 1999, a: 99, b: 3 });
    assert.deepEqual( Object.entries(sort({ age: 22, year: 1999, a: 99, b: 3 }) ), Object.entries({
      b: 3,
      age: 22,
      a: 99,
      year: 1999,
    })
    );
  });
});
