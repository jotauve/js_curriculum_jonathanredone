
// write a function called takeAll which takes an array as argument,
// it then returns a new array which has all values of the original
// array but in reverse order.


const takeAll = (array,i)=>{
    var newArr = []
    newArr = array.reverse()
    return newArr
}


module.exports = {
    takeAll,
}
