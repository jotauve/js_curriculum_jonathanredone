// write a function called isString which takes an array as argument.
// inside it write a forEach loop that loops through this example array and 
// checks if the type of each element is a string, 
// every time it finds a string it pushes it into an array.
// return that array
// you can refer to the example in EXERCISE 3 for the simple if statement if you wish.

var anyArr = ['one', 5, 'two', 6, 'three', true, 'four']

const isString=(anyArr)=>{
    arr2=[]
anyArr.forEach(ele=>{
 if (typeof ele == 'string') 
      arr2.push(ele)
})
return arr2
}



module.exports = {
    isString
}

