// declare the following array

// create a function called looper which takes the array as argument
// inside it write a forEach loop which console.log each element and each 
// index for every iteration.
// Inside the function declare a variable called count, and inside the loop increment it by one 
// for each iteration then return count.


var arr = ['one', 'two', 'three', 'four']
const looper=(arr)=>{
  count= 0
arr.forEach((ele, index)=>{
  console.log( ele, index)
  count ++
})
return count
}

module.exports = {
  looper, arr
}