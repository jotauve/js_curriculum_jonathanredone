// write a function called tellAge that tells you how old you are based on the date of birth passed.
// this function will take 3 arguments month, day and year
// You should use the Date object to set the value of today.
// if the birth date is less than one year from the current date it should return : "You are NUMBER_DAYS old"
// if its more than one year it should return you are NUMBER_YEARS old"
// Info on Date object: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date



const tellAge=(month, day, year)=>{
var today = new Date()
var birth = new Date(`${month}/${day}/${year}`)
var timeDiff =today - birth
var DaysAlive= Math.floor( timeDiff/86400000) 
if (DaysAlive < 365){
    return `You are ${DaysAlive} days old`
}
else if (DaysAlive >= 365){
    return `You are ${Math.floor(DaysAlive/365)} years old`
}
}
module.exports = {
    tellAge
}