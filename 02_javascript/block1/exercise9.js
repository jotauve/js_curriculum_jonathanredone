/*
You have a function 'getAge' which takes 3 arguments: year of birth, current year, name and returns a phrase "Hello
 USER_NAME you are NUMBER years old" where USER_NAME is the third argument and NUMBER is the user's age calculated from the first and the second arguments.

*/   
   
   
   var getAge  = function (birth_year, now, name) {
       var age = now-birth_year
        return `Hello ${name} you are ${age} years old`
    }

    module.exports = {getAge}