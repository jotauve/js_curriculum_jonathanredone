This is your file structure for the exercises and projects of JavaScript Full-Stack Bootcamp.

The description of exercises could be found inside corresponding folders.

To simplify GitLab workflow we suggest to take 01_html_css folder outside of you BCS_JS_BOOTCAMP_FILES_TDD since each cloning project from HTML/CSS section could be treated as separate projects with their own GitLab repo.

JavaScript, React and Express exercises could be pushed all together with the main repo (`git push` from the BCS_JS_BOOTCAMP_FILES_TDD folder).

Have fun!!!

(c) Barcelona Code School
