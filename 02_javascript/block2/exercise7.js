/*
Write a function called pusher which takes one argument - an array.

Using .push() method take all the elements from the array and push them to a new variable called arr2 avoiding
having nested arrays. Then return arr2.

hint==> you might want to take a look at spread operator: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
*/

const pusher=(array)=>{
	var arr2=[]
	arr2.push(...array)
	return arr2
}




module.exports = {
	pusher,
}
