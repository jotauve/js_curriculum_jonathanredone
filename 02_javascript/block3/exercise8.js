// write a function called timesTwo which takes an array as an argument.
// inside it use the forEach loop and push the numbers of the array to a 
// new array and multiply them by two on the way then return the new array

const timesTwo=(array)=>{
    newArr=[]
    array.forEach(ele=>{
        newArr.push(ele*2)
    })
    return newArr
}

module.exports ={
    timesTwo
}