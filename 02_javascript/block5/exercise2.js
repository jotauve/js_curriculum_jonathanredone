
// extend our awesome calc function by adding some conditions...
// in case the third arguments is  / or * and the second argument is not provided,
// the second argument should default to one.
// in case the third arguments is +  or  - and the second argument is not provided,
// the second argument should default to zero.


const calc=(numb,numb2,sign)=>{

    if(numb2 === '+'||numb2 === '-'){
        sign = numb2
        numb2 = 0
    }
    if(numb2 === '*'||numb2 === '/'){
        sign = numb2
        numb2 = 1
    }
    if (sign == '+'){
        return numb + numb2}
    else if (sign == '-'){
        return numb - numb2}
    else if (sign == '*'){
        return numb * numb2}
    else if (sign == '/'){
        return numb / numb2}
    else {
        return 'wrong data provided'}
    }
      module.exports = {
         calc
     }
