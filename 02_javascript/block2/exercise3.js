/*
Create a function swap which takes 3 arguments -- two arrays and a number. It needs to swap two items in these arrays at the specified position/index which is the third argument -- number. Then this function has to return these two arrays inside another array.

In the example we are swapping orange with playstation because the provided index is 2.
*/

const swap=(array, array2,i)=>{
	var safeArea = array[i]
	array[i] = array2[i]
	array2[i] = safeArea
	newArr=[array,array2]
 return newArr
}


module.exports = {
	swap,
}
