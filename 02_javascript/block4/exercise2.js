// write a function  called is_an_even_number that takes an array as argument, 
// it loops through it and checks if the elements are numbers (or a string that can be converted to a number) 
// and if they are even.
// you should also keep track of the even numbers by increasing the value of a variable called count, which you should 
// return at the end of the function.
// for this exercise we treat number 0 as an even number, but only if it is provided as a zero

const is_an_even_number=(array)=>{
  var evenCount=0
  for(i=0;i<array.length;i++)
  if(Number(array[i])== Number && ((Number(array[i]) % 2)==0)){
    evenCount++
  }else if (Number(array[i]) == 'zero'){
    evenCount++
  }

return evenCount
}

module.exports = {
  is_an_even_number
}