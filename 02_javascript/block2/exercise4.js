// create a function called splicer which removes an element from an array and returns this modified array.
// this function takes two arguments:
// 1) the array
// 2) the position/index of the element in the array to be removed


const splicer=(array,i)=>{
array.splice(i,1)
return array
}


module.exports = {
    splicer,
}
