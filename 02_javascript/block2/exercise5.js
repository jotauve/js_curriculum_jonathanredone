/* 
Write a function called removeFirstAndLast. It takes an array as an argument and returns it but without first and last items which should be removed.
*/


const removeFirstAndLast=(array)=>{
array.shift()
array.pop()
return array
}

module.exports = {
    removeFirstAndLast
}