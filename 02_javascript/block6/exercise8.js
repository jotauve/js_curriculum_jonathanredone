// Write a function called last that takes one argument, an object, and returns an object containing 
// only the last key/value pair and does not modify the original object.

//======================  EXAMPLE  ========================
// var obj = {a: 20, b: 26, c: 0}
// last(obj)
// {c:0} // <======  EXPECTED OUTPUT
// //{a: 20, b: 26, c: 0}  <====== obj
//=========================================================  
var object = {a: 20, b: 26, c: 0}

const last = (object) => {
var fin = Object.entries(object)
var final : {}
final: (fin[fin.length-1])
 return final
}






module.exports ={
    last
}
