/*
Make a function which takes age of a user and minimum age for driving a scooter and returns true if user is old enough to drive a scooter and false if not


*/

var checkAge = function (age, minAge){
    var legal = age > minAge
    return legal
}

module.exports = {
    checkAge
}