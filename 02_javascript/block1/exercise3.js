
/*
Age calculator

Want to find out how old you'll be? Calculate it!

You have a function which takes 2 arguments -- year of birth and current year.

Finish the function so that it will return the correct age.
*/



var ageCalc = function (date_of_birth, future_year) {
	// your code goes here
    age=(future_year-date_of_birth)
	// and return something :)
	return age
}

module.exports = {
	ageCalc,
}
