// write a function called check_types it takes an array as argument, 
//it loops through it and checks the typeof each element
// it needs to find out how many different data types there are.
// it should at the end return the number of data types.

const check_types=(array)=>{
var countArr=[]
array.forEach((ele,ind)=>{
  if (!countArr.includes(typeof ele)){
    countArr.push(typeof ele)
  }
})
return countArr.length
}


module.exports = {
  check_types
}