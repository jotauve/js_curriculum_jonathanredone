var assert = require("chai").assert;
var isTrue = require("../block4/exercise0").isTrue;

describe("#test0", function () {
  it("isTrue should be a function", function () {
    assert.typeOf(isTrue, "function");
  });
});
describe("#test1", function () {
  it("isTrue should be true for 'I exist!'", function () {
    assert.equal(isTrue("I exist!"), true);
  });
});
describe("#test2", function () {
  it("isTrue should be false for ''", function () {
    assert.equal(isTrue(""), false);
  });
});
describe("#test3", function () {
  it("isTrue should be false for false", function () {
    assert.equal(isTrue(false), false);
  });
});
describe("#test4", function () {
  it("isTrue should be false for null", function () {
    assert.equal(isTrue(null), false);
  });
});
describe("#test5", function () {
  it("isTrue should be false for undefined", function () {
    assert.equal(isTrue(undefined), false);
  });
});
describe("#test6", function () {
  it("isTrue should be false for 0", function () {
    assert.equal(isTrue(0), false);
  });
});
describe("#test7", function () {
  it("isTrue should be true for 10", function () {
    assert.equal(isTrue(10), true);
  });
});
describe("#test8", function () {
  it("isTrue should be true for {}", function () {
    assert.equal(isTrue({}), true);
  });
});
describe("#test9", function () {
  it("isTrue should be true for []", function () {
    assert.equal(isTrue([]), true);
  });
});
describe("#test7", function () {
  it("isTrue should be true for ()=>{}", function () {
    assert.equal(isTrue(()=>{}), true);
  });
});
describe("#test7", function () {
  it("isTrue should be false for NaN", function () {
    assert.equal(isTrue(NaN), false);
  });
});